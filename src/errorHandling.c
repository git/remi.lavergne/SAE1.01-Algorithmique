/**
 * @file errorHandling.c
 * @brief Gestion des erreurs
*/

#include "main.h"

/**
 * @brief Fonction d'affichage des erreurs
 * @param error: Code d'erreur
 * @return void
*/
void errorHandling(int error)
{
    //? Exemple d'erreurs (à adapter)
    if (error < 0)
    {
        printf("\a \x1B[31m"); // Bip d'erreur
        switch (error)
        {
            case -1:
                printf("[ERREUR] - L'utilisateur a quitté.\n");
                break;
            case -2:
                printf("[ERREUR] - La taille physique du tableau est dépassée.\n");
                break;
            case -3:
                printf("[ERREUR] - L'espace disponible du tableau est insuffisant.\n");
                break;
            case -4:
                printf("ERREUR] - Problème lors de la lecture d'un fichier.\n");
                exit(1);
            case -5:
                printf("[ERREUR] - Problème lors de l'écriture d'un fichier.\n");
                exit(2);
            case -6:
                printf("[ERREUR] - Le champ renseigné doit être positif.\n");
                break;
            case -7:
                printf("[ERREUR] - L'identifiant n'existe pas.\n");
                break;
            case -8:
                printf("[ERREUR] - La référence existe déjà.\n");
                break;
            case -9:
                printf("[ERREUR] - Le mot de passe est incorrect.\n");
                break;
            case -10:
                printf("[ERREUR] - Votre compte est suspendu.\n");
                break;
            case -11:
                printf("[ERREUR] - Le client n'est pas administrateur.\n");
                break;
            case -12:
                printf("[ERREUR] - Vous ne pouvez pas supprimer plus d'articles que vous n'en avez dans votre panier, réessayez.\n");
                break;
            case -13:
                printf("[ERREUR] - Le client n'a pas assez d'argent sur sa cagnotte.\n");
                break;
            case -14:
                printf("[ERREUR] - vous ne pouvez pas supprimer un nombre nul ou négatif d'articles, réessayez.\n");
                break;
            default:
                printf("[ERREUR] - Une erreur s'est produite.\n");
                break;
        }
        printf("\x1B[0m");
    }
}
