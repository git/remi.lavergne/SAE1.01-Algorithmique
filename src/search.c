/**
 * @file search.c
 * @brief Fonctions de recherche de client et d'article.
 * @version 1.0
 * @date 2023-10-30
 * @warning Ne pas utiliser les fonctions de recherche si les tableaux ne sont pas triés.
*/

#include "main.h"

/**
 * @brief Recherche d'une valeur dans un tableau cible (trié par ordre croissant).
 * @param targetTab: tableau cible
 * @param valSearched: valeur recherchée
 * @param tLog: taille logique du tableau cible
 * @param found: pointeur sur un entier qui indique si la valeur a été trouvée ou non
 * @return index de la valeur recherchée dans le tableau cible ou celle d'insertion si non trouvée
*/
int searchTab(int targetTab[], int valSearched, int tLog, int *found)
{
    for(int i=0; i < tLog; i++)
    {
        if(targetTab[i] == valSearched)
        {
            *found = 1;
            return i;
        }

        if(targetTab[i] > valSearched)
        {
            *found = 0;
            return i;
        }
    }
    *found = 0;
    return -1;
}