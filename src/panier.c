/**
 * @file panier.c
 * @brief Fonctions de gestion du panier client et des contraintes
*/

#include "main.h"


/**
 * @brief La fonction `clientConstraint` demmande à l'utilisateur d'entrer la charge limite et le volume maximal
 * du véicule.
 * @param weight Pointeur float qui stocke la charge maximale (en Kg) du véicule.
 * @param volume Pointeur float qui stocke le volume maximal (en metres cube) du véicule.
 * @param price Pointeur float qui permet de stocker le budget maximum du client.
 */

void clientConstraint(float *weight, float *volume, float *price)
{
	// Saisie des contraintes du client.
	printf("Entrez la charge maximale de votre véhicule en kilogrammes: ");
	scanf("%f%*c", weight);
	while (*weight < 0)
	{
		printf("Erreur, le poids doit être positif, entrez la charge maximale de votre véhicule en kilogrammes: ");
		scanf("%f%*c", weight);
	}

	printf("Entrez le volume du coffre de votre véhicule en mètres cubes: ");
	scanf("%f%*c", volume);
	while (*volume < 0)
	{
		printf("Erreur: le volume doit être positif, entrez le volume du coffre de votre véhicule en mètres cubes: ");
		scanf("%f%*c", volume);
	}

	printf("Entrez votre budget maximum ou -1 si vous n'en avez pas: ");
	scanf("%f%*c", price);
	while (*price < 0 && *price != -1)
	{
		printf("Erreur: le prix doit être positif, entrez votre budget maximum ou -1 si vous n'en avez pas: ");
		scanf("%f%*c", price);
	}
}



/**
 * @brief constraintExceeded permet de regarder si la charge maximale, le volume maximal, et le prix maximal sont 
 * dépassés à partir des articles dans le panier
 * 
 * @param weightConstraint Float qui stocke la charge maximale (en Kg) du véicule.
 * @param volumeConstraint Float qui stocke le volume maximal (en metres cube) du véicule.
 * @param priceConstraint Pointeur float qui permet de stocker le budget maximum du client.
 * @param cagnotte Pointeur float qui stocke la cagnotte du client.
 * @param tabWeight Tableau float qui stocke le poids des articles.
 * @param tabVolume Tableau float qui stocke le volume des articles.
 * @param tabPrice Tableau float qui stocke le prix des articles.
 * @param tabItemRef Tableau int qui stocke les références des articles.
 * @param tabBasketRef Tableau contenant les références des articles dans le panier.
 * @param tabBasketQuantity Tableau stockant la quantité commandée des articles dans le panier.
 * @param tlogItem Taille logique du tableau `tabItemRef`.
 * @param tlogBasket Taille logique du tableau `tabBasketRef`.
 * @return Renvoie 0 si tout ce passe sans erreur, renvoie -1 en cas d'erreur.
 */

int constraintExceeded(float weightConstraint, float volumeConstraint, float *priceConstraint, float *cagnotte, float tabWeight[], float tabVolume[], float tabPrice[], int tabItemRef[], int tabBasketRef[], int tabBasketQuantity[], int tlogItem, int tlogBasket)
{
	int index, found, i;
	float totalWeight = 0, totalVolume = 0, totalPrice = 0, cagnotteUse;
	for (i=0; i<tlogBasket; i++)
	{
		index = searchTab(tabItemRef, tabBasketRef[i], tlogItem, &found);
		if (found == 1)
		{
			totalWeight += tabWeight[index] * tabBasketQuantity[i];
			totalVolume += tabVolume[index] * tabBasketQuantity[i];
			if (*priceConstraint != -1)
				totalPrice += tabPrice[index] * tabBasketQuantity[i];
			if (totalWeight > weightConstraint)
			{
				printf("Attention, la contrainte de charge maximale du véhicule a été dépassée de %.2f, veuillez retirer un article\n", totalWeight - weightConstraint);
				return -1;
			}
			if (totalVolume > volumeConstraint)
			{
				printf("Attention, la contrainte de volume maximum a été dépassé de %.2f, veuillez retirer un article\n", totalVolume - volumeConstraint); 
				return -1;
			}
			if (*priceConstraint != -1 && totalPrice > *priceConstraint)
			{
				printf("Attention, la contrainte de budget maximum a été dépassé de %.2f\n", totalPrice - *priceConstraint);
				if (*cagnotte < totalPrice)
				{
					printf("Le prix total excédant votre cagnotte, veuillez retirer un article\n");
					return -1;
				}
				else
				{
					printf("Vous pouvez utiliser votre cagnotte, dont le montant est de %.2f, entrez le montant à utiliser ou -1 si vous voulez retirer un article: ", *cagnotte);
					scanf("%f%*c", &cagnotteUse);
					if (cagnotteUse == -1)
						return -1;
					
					while (cagnotteUse > *cagnotte || totalPrice-cagnotteUse > *priceConstraint)
					{
						if (cagnotteUse > *cagnotte)	
							printf("Erreur: vous ne pouvez pas utiliser plus que vous n'avez, réessayez ou entrer -1 si vous souhaitez retirer un article: ");
						if (totalPrice-cagnotteUse > *priceConstraint)
							printf("Erreur: vous n'avez pas utilisé assez de votre cagnotte, vous devez utiliser au moins %.2f, veuillez rentrer le montant à utiliser ou -1 si vous voulez retirer un article: ", totalPrice - *priceConstraint);
						scanf("%f%*c", &cagnotteUse);
						if (cagnotteUse == -1)
							return -1;
						*cagnotte -= cagnotteUse;
						*priceConstraint += cagnotteUse;
					}
				}
			}
		}
	}
	return 0;
}

/**
 * @brief Ajoute un article au panier et calcule les différents attributs pour la fonction `display_basket`.
 * @param tab_reference: tableau des références des articles.
 * @param weight: tableau des poids des articles.
 * @param volume: tableau du volume des articles.
 * @param unitPrice: tableau du prix unitaire des articles.
 * @param cagnotte: pointeur indiquant la valeur de la cagnotte.
 * @param basket_tab_ref: tableau des références du panier.
 * @param basket_tab_qte: tableau de la quantité de l'article du panier.
 * @param tlog: taille logique du tableau `tab_reference`.
 * @param tlog_basket: taille logique du panier.
*/
void basket_add(int tab_reference[], float unitPrice[], float *cagnotte, int  basket_tab_ref[], int basket_tab_qte[], int tlogItem, int *tlog_basket)
{
	int i, ref_to_add, qte_to_add, trouve, index_ajout;
	float total_weight[tmaxArticles], total_volume[tmaxArticles], total_price[tmaxArticles], total_cagnotte[tmaxArticles];
	if (tlogItem+1>tmaxArticles)
	{
		printf("erreur, pas assez de place pour ajouter un autre article");
		return;
	}
	printf("Quelle référence souhaitez-vous ajouter au panier?");
	scanf("%d%*c", &ref_to_add);
	index_ajout = searchTab(tab_reference, ref_to_add, tlogItem, &trouve);
	while (trouve == 0)
	{
		printf("L'élément que vous souhaitez ajouter n'existe pas, ressayez s'il vous plaît");
		scanf("%d", &ref_to_add);
		index_ajout = searchTab(tab_reference, ref_to_add, tlogItem, &trouve);
	}
	basket_tab_ref[index_ajout] = ref_to_add;
	printf("Quelle quantité de cet article souhaitez-vous ajouter au panier?");
	scanf("%d%*c", &qte_to_add);
	while (qte_to_add <= 0)
	{
		printf("Vous ne pouvez pas ajouter une quantité nulle ou négative ressayez");
		scanf("%d%*c", &qte_to_add);
	}
	for (i=*tlog_basket; i>=index_ajout; i--)
			{
				basket_tab_ref[i]=basket_tab_ref[i+1];
				basket_tab_qte[i]=basket_tab_qte[i+1];
			}
	basket_tab_qte[index_ajout] = ref_to_add;
	
	*cagnotte += (unitPrice[index_ajout]*qte_to_add) *0.1;
	*tlog_basket += 1;
}

/**
 * @brief Supprime le panier en lui donnant sa taille_logique à 0.
 * @param tlog_basket: taille logique du panier.
 * @return taille logique du panier.
*/
void reinit_basket(int *tlog_basket)
{
	*tlog_basket=0;
	// En mettant tlog_basket à 0,
	// on fait comme si la taille logique était à 0, faisant que l'on ne considère plus aucun élément des tableaux, 
	//et donc qu'il est désormais vide.
}

/**
 * @brief Supprime un article au panier ou modifie sa quantité si la quantité est supérieure à 1.
 * @param basket_tab_ref: tableau des références du panier.
 * @param basket_tab_qte: tableau de la quantité de l'article du panier.
 * @param tlog_basket: taille logique du panier.
 * @return taille logique du panier.
*/
void basket_del_article(int basket_tab_ref[], int basket_tab_qte[], int *tlog_basket)
{
	int ref_to_del, trouve, index_to_del, qte_to_del, i;

	printf("Quelle référence voulez vous supprimer de votre panier?\n");
	scanf("%d%*c",&ref_to_del);
	index_to_del = searchTab(basket_tab_ref, ref_to_del, *tlog_basket, &trouve);
	while (trouve == 0)
	{
		printf("Erreur, la valeur que vous voulez supprimer n'existe pas, réssayez");
		scanf("%d%*c",&ref_to_del);
		index_to_del = searchTab(basket_tab_ref, ref_to_del, *tlog_basket, &trouve);
	}
	if (basket_tab_qte[index_to_del]>1)
	{
		printf("Combien d'articles e ce type voulez-vous supprimer?");
		scanf("%d%*c",&qte_to_del);
		while(qte_to_del<=0)
		{
			errorHandling(-14);
			scanf("%d%*c",&qte_to_del);
		}
		while(basket_tab_qte[index_to_del]-qte_to_del>=0)
		{
			errorHandling(-12);
			scanf("%d%*c",&qte_to_del);
		}
		if (qte_to_del<basket_tab_qte[index_to_del])
		{
			basket_tab_qte[index_to_del]=basket_tab_qte[index_to_del]-qte_to_del;
		}
		else if (qte_to_del==basket_tab_qte[index_to_del])
		{
			for (i=index_to_del; i<*tlog_basket; i++)
			{
				basket_tab_ref[i]=basket_tab_ref[i+1];
				basket_tab_qte[i]=basket_tab_qte[i+1];
			}
			*tlog_basket -= 1;
		}
		
	}

}