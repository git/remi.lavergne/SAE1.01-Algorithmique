/**
 * @file traitement.c
 * @brief Traitement des données depuis les fichiers. Chargement - Sauvegarde
*/

#include "main.h"

/**
 * @brief Chargement des données depuis les 2 fichiers vers les tableaux
 * @param tLogArticle: Taille logique du tableau des articles
 * @param tLogClient: Taille logique du tableau des clients
 * @param reference: Tableau des références des articles
 * @param weight: Tableau des poids des articles
 * @param volume: Tableau des volumes des articles
 * @param unitPrice: Tableau des prix à l'unité des articles
 * @param clientID: Tableau des identifiants des clients
 * @param clientPassword: Tableau des mots de passe des clients
 * @param cagnotte: Tableau des cagnottes des clients
 * @param suspended: Tableau de l'état du compte des clients
 * @param isAdmin: Tableau de l'état d'administrateur des clients
 * @return void
*/
void chargeDonnees(int *tLogArticle, int *tLogClient,int reference[], float weight[], float volume[], float unitPrice[], int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[])
{
    *tLogArticle = chargeArticles(reference, weight, volume, unitPrice);
    *tLogClient = chargeClients(clientID, clientPassword, cagnotte, suspended, isAdmin);
}

/**
 * @brief Chargement des données du fichier des articles vers les tableaux
 * @param reference: Tableau des références des articles
 * @param weight: Tableau des poids des articles
 * @param volume: Tableau des volumes des articles
 * @param unitPrice: Tableau des prix à l'unité des articles
 * @return Taille logique des tableaux d'articles
*/
int chargeArticles(int reference[], float weight[], float volume[], float unitPrice[])
{
    FILE *article;
    article = fopen("articles.txt","r");
    if(article==NULL)
    {
        errorHandling(-4);
    }

    int ref, tL=0;
    float w, v, up;
    
    fscanf(article, "%d %f %f %f", &ref, &w, &v, &up);
    while(!feof(article))
    {
        if(tL == tmaxArticles)
        {
            errorHandling(-3);
            return tL;
        }

        reference[tL] = ref;
        weight[tL] = w;
        volume[tL] = v;
        unitPrice[tL] = up;

        tL++;

        fscanf(article, "%d %f %f %f", &ref, &w, &v, &up);
    }

    fclose(article);
    return tL;
}

/**
 * @brief Chargement des données du fichier des clients vers les tableaux
 * @param clientID: Tableau des identifiants des clients
 * @param clientPassword: Tableau des mots de passe des clients
 * @param cagnotte: Tableau des cagnottes des clients
 * @param suspended: Tableau de l'état du compte des clients
 * @param isAdmin: Tableau de l'état d'administrateur des clients
 * @return Taille logique des tableaux des clients
*/
int chargeClients(int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[])
{
    FILE *client;
    client = fopen("clients.txt","r");
    if(client==NULL)
    {
        errorHandling(-4);
    }

    int id, passwd, sus, tL=0, adm;
    float cag;
    
    fscanf(client, "%d %d %f %d %d", &id, &passwd, &cag, &sus, &adm);
    while(!feof(client))
    {
        if(tL == tmaxArticles)
        {
            errorHandling(-3);
            return tL;
        }

        clientID[tL] = id;
        clientPassword[tL] = passwd;
        cagnotte[tL] = cag;
        suspended[tL] = sus;
        isAdmin[tL] = adm;

        tL++;

        fscanf(client, "%d %d %f %d %d", &id, &passwd, &cag, &sus, &adm);
    }

    fclose(client);
    return tL;
}

/**
 * @brief Sauvegarde des données des tableaux vers les 2 fichiers correspondants
 * @param tLogArticle: Taille logique du tableau des articles
 * @param tLogClient: Taille logique du tableau des clients
 * @param reference: Tableau des références des articles
 * @param weight: Tableau des poids des articles
 * @param volume: Tableau des volumes des articles
 * @param unitPrice: Tableau des prix à l'unité des articles
 * @param clientID: Tableau des identifiants des clients
 * @param clientPassword: Tableau des mots de passe des clients
 * @param cagnotte: Tableau des cagnottes des clients
 * @param suspended: Tableau de l'état du compte des clients
 * @param isAdmin: Tableau de l'état d'administrateur des clients
 * @return void
*/
void sauvegardeDonnees(int tLogArticle, int tLogClient, int reference[], float weight[], float volume[], float unitPrice[], int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[])
{
    printf("[DEBUG] - Sauvegarde des données...\n");
    sleep(2);
    sauvegardeArticles(reference, weight, volume, unitPrice, tLogArticle);
    sauvegardeClients(clientID, clientPassword, cagnotte, suspended, isAdmin, tLogClient);
}

/**
 * @brief Sauvegarde des données des articles de tableaux vers le fichier des articles
 * @param reference: Tableau des références des articles
 * @param weight: Tableau des poids des articles
 * @param volume: Tableau des volumes des articles
 * @param unitPrice: Tableau des prix à l'unité des articles
 * @param tLogArticle: Taille logique des tableaux des articles
 * @return void
*/
void sauvegardeArticles(int reference[], float weight[], float volume[], float unitPrice[], int tLogArticle)
{
    FILE *article;
    article = fopen("articles.txt","w");
    if(article==NULL)
    {
        errorHandling(-5);
    }

    int i;
    for(i=0; i<tLogArticle; i++)
    {
        fprintf(article, "%d %f %f %f\n", reference[i], weight[i], volume[i], unitPrice[i]);
    }

    printf("[DEBUG] - Sauvegarde articles réussie.\n");
    fclose(article);
}

/**
 * @brief Sauvegarde des données des clients de tableaux vers le fichier des clients
 * @param clientID: Tableau des identifiants des clients
 * @param clientPassword: Tableau des mots de passe des clients
 * @param cagnotte: Tableau des cagnottes des clients
 * @param suspended: Tableau de l'état du compte des clients
 * @param isAdmin: Tableau de l'état d'administrateur des clients
 * @param tLogClient: Taille logique des tableaux des clients
 * @return void
*/
void sauvegardeClients(int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[], int tLogClient)
{
    FILE *client;
    client = fopen("clients.txt","w");
    if(client==NULL)
    {
        errorHandling(-5);
    }

    int i;
    for(i=0; i<tLogClient; i++)
    {
        fprintf(client, "%d %d %f %d %d\n", clientID[i], clientPassword[i], cagnotte[i], suspended[i], isAdmin[i]);
    }

    printf("[DEBUG] - Sauvegarde clients réussie.\n");
    fclose(client);
}