/**
 * @file menu.c
 * @brief Menu client et administrateur
*/

#include "main.h"

/**
 * @brief La fonction `clientMenu` affiche un menu au client et lui laisse choisir d'effectuer 
 * des actions sur son panier.
 * 
 * @param weight Pointeur float qui stocke la charge maximale (en Kg) du véicule.
 * @param volume Pointeur float qui stocke le volume maximal (en metres cube) du véicule.
 * @param price Pointeur float qui permet de stocker le budget maximum du client.
 * @param tabItemRef Tableau int qui stocke les références des articles.
 * @param unitPrice Tableau float qui stocke le prix des articles.
 * @param cagnottePointeur Pointeur float qui stocke la cagnotte du client.
 * @param basket_tab_ref Tableau contenant les références des articles dans le panier.
 * @param basket_tab_qte Tableau stockant la quantité commandée des articles dans le panier.
 * @param tabWeight Tableau float qui stocke le poids des articles.
 * @param tabVolume Tableau float qui stocke le volume des articles.
 * @param tlogItem Taille logique du tableau `tabItemRef`.
 * @param tlogBasket Taille logique du tableau `tabBasketRef`.
 */
void clientMenu(float *weight, float *volume, float *price, int tabItemRef[], float unitPrice[], float *cagnotte, int basket_tab_ref[], int basket_tab_qte[], float tabWeight[], float tabVolume[], int tlogItem, int *tlogBasket)
{
	int choice = 0, constraint;
    while (choice != 6)
    {
		printf("\nTapez sur Entrée pour continuer...");
		if (getchar() == '\n') // getchar() pour récupérer le caractère entré par l'utilisateur
		{
			system("clear"); // Clear le terminal
		}
		
    	printf("\n=================================\n");
	    printf("          Menu Client\n");
	    printf("=================================\n");
	    printf("1\u2022 Entrer ses contraintes\n");
	    printf("2\u2022 Ajouter un article au panier\n");
	    printf("3\u2022 Afficher le panier\n");
	    printf("4\u2022 Réinitialiser le panier\n");
	    printf("5\u2022 Retirer un article du panier\n");
	    printf("6\u2022 Déconnexion\n");
	    printf("=================================\n");
	    printf("Votre choix: ");
	    scanf("%d%*c", &choice);

		system("clear");

    	switch (choice)
    	{
    		case 1:
    			clientConstraint(weight, volume, price);
    			break;
    		case 2:
				basket_add(tabItemRef, unitPrice, cagnotte, basket_tab_ref, basket_tab_qte, tlogItem, tlogBasket);
				constraint = constraintExceeded(*weight, *volume, price, cagnotte, tabWeight, tabVolume, unitPrice, tabItemRef, basket_tab_ref, basket_tab_qte, tlogItem, *tlogBasket);
				if (constraint == -1)
					basket_del_article(basket_tab_ref, basket_tab_qte, tlogBasket);
    			break;
    		case 3:
				display_basket(basket_tab_ref, tabItemRef, basket_tab_qte, tabWeight, tabVolume, *cagnotte, unitPrice, tlogItem, *tlogBasket);
    			break;
    		case 4:
				reinit_basket(tlogBasket);
    			break;
    		case 5:
				basket_del_article(basket_tab_ref, basket_tab_qte, tlogBasket);
    			break;
    		case 6:
    			return;
    		default:
    			printf("Erreur, veuillez entrer un choix valide.\n");
    			break;
    	}
    }
}

/**
 * @brief La fonction `adminMenu` permet à l'utilisateur administrateur de pouvoir utiliser des fonctions permettant
 * d'ajouter/supprimer/modifier des articles du panier/ des clients, et afficher les informations
 * du client/de l'article.
 * 
 * @param clientID Tableau int qui stocke les identifiants des clients.
 * @param clientPassword Tableau int qui stocke les mots de passe des clients.
 * @param cagnotte Tableau float qui stocke les cagnonttes de chaque client.
 * @param suspended Tableau int qui stocke l'état de suspention du compte des clients.
 * Suspendu = 1, 0 sinon.
 * @param isAdmin Tableau int qui indique si l'utilisateur de l'application est administrateur.
 * Administrateur = 1, 0 sinon.
 * @param tabReference Tableau int qui stocke les références des articles.
 * @param tabWeight Tableau float qui stocke le poids des articles.
 * @param tabVolume Tableau float qui stocke le volume des articles.
 * @param unitPrice Tableau float qui stocke le prix des articles.
 * @param tlogItem Taille logique du tableau `tabItemRef`.
 * @param tlogClient Taille logique du tableau `clientID`.
 */
void adminMenu(int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[], int tabReference[], float tabWeight[], float tabVolume[], float unitPrice[], int *tlogItem, int *tlogClient)
{
	int choice = 0;
	while (choice != 10)
	{
		printf("\nTapez sur Entrée pour continuer...");
		if (getchar() == '\n') // getchar() pour récupérer le caractère entré par l'utilisateur
		{
			system("clear"); // Clear le terminal
		}

		printf("\n=================================\n");
	    printf("          Menu Admin\n");
	    printf("=================================\n");
		printf("1\u2022 Ajouter un client\n");
		printf("2\u2022 Modifier les données d'un client\n");
		printf("3\u2022 Supprimer un client\n");
		printf("4\u2022 Ajouter un article\n");
		printf("5\u2022 Supprimer un article\n");
		printf("6\u2022 Afficher les données d'un article\n");
		printf("7\u2022 Afficher la liste des articles\n");
		printf("8\u2022 Afficher les données d'un client\n");
		printf("9\u2022 Afficher la liste des clients\n");
	    printf("10\u2022 Déconnexion\n");
	    printf("=================================\n");
	    printf("Votre choix: ");
		scanf("%d%*c", &choice);
		
		system("clear");

		switch (choice)
		{
			case 1:
				signup(clientID, clientPassword, cagnotte, suspended, isAdmin, tlogClient);
				break;
			case 2:
				modifyClient(clientID, cagnotte, suspended, isAdmin, *tlogClient);
				break;
			case 3:
				deleteClient(clientID, clientPassword, cagnotte, suspended, isAdmin, tlogClient);
				break;
			case 4:
				inputItem(tabReference, tabWeight, tabVolume, unitPrice, tlogItem);
				break;
			case 5:
				deleteItem(tabReference, tabWeight, tabVolume, unitPrice, tlogItem);
				break;
			case 6:
				displayItem(tabReference, tabWeight, tabVolume, unitPrice, *tlogItem);
				break;
			case 7:
				displayItemList(tabReference, tabWeight, tabVolume, unitPrice, *tlogItem);
				break;
			case 8:
				displayClient(clientID, cagnotte, suspended, isAdmin, *tlogClient);
				break;
			case 9:
				displayClientList(clientID, cagnotte, suspended, isAdmin, *tlogClient);
				break;
			case 10:
				return;
			default:
				printf("Erreur, veuillez entrer un choix valide.\n");
				break;
		}
	}
}

/**
 * @brief Pour faire opposition et débloquer son compte lorsqu'il est suspendu.
 * @param clientID Tableau des identifiants client
 * @param clientPassword Tableau des mots de passe
 * @param suspended Tableau des états de compte
 * @param tlogClient Taille logique du tableau des clients
 * 
 * @return void
*/
void opposition(int clientID[], int clientPassword[], int suspended[], int tlogClient)
{
	int id, password, index, found;
	printf("Entrez votre identifiant: ");
	scanf("%d%*c", &id);
	index = searchTab(clientID, id, tlogClient, &found);
	if (found == 0)
	{
		errorHandling(-7);
		return;
	}

	if(suspended[index] == 0)
	{
		printf("Votre compte n'est pas suspendu.\n");
		return;
	}

	printf("Entrez votre mot de passe: ");
	scanf("%d%*c", &password);
	if (password == clientPassword[index])
	{
		suspended[index] = 0;
		printf("Votre compte a été réactivé.\n");
	}
	else
	{
		errorHandling(-9);
	}
}