/**
 * @file decalage.c
 * @brief Fonctions de décalage d'un tableau
 * 
 * Contient les fonctions de décalage de tableau vers la droite (pour l'insertion)
 * et vers la gauche (pour la suppression).
*/

#include "main.h"

void decalageADroiteInt(int tab[], int index, int tlog)
{
    for (int i = tlog; i > index; i--)
        tab[i] = tab[i-1];
}

void decalageAGaucheInt(int tab[], int index, int tlog)
{
    for (int i = index; i < tlog; i++)
        tab[i] = tab[i+1];
}

void decalageADroiteFloat(float tab[], int index, int tlog)
{
    for (int i = tlog; i > index; i--)
        tab[i] = tab[i-1];
}

void decalageAGaucheFloat(float tab[], int index, int tlog)
{
    for (int i = index; i < tlog; i++)
        tab[i] = tab[i+1];
}