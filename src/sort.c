/**
 * @file sort.c
 * @brief Fonctions de tri des tableaux de données (articles et clients).
 * @version 1.0
 * @date 2023-10-30
*/

#include "main.h"

/**
 * @brief Tri des clients par ordre croissant de leur identifiant (clientID).
 * @param clientID: tableau des identifiants des clients
 * @param cagnotte: tableau des cagnottes des clients
 * @param suspended: tableau de l'état du compte des clients
 * @param isAdmin: tableau de l'état d'administrateur des clients
 * @param tlog: taille logique des tableaux des clients
*/
int sortClient(int clientID[], float cagnotte[], int suspended[], int isAdmin[], int tlog)
{
    int i, j, tempID, tempSuspended, tempIsAdmin;
    float tempCagnotte;
    for (i=0; i<tlog; i++)
    {
        for (j=0; j<tlog; j++)
        {
            if (clientID[j] > clientID[j+1])
            {
                // Variables temporaires pour le tri
                tempID = clientID[j];
                tempCagnotte = cagnotte[j];
                tempSuspended = suspended[j];
                tempIsAdmin = isAdmin[j];

                clientID[j] = clientID[j+1];
                cagnotte[j] = cagnotte[j+1];
                suspended[j] = suspended[j+1];
                isAdmin[j] = isAdmin[j+1];

                clientID[j+1] = tempID;
                cagnotte[j+1] = tempCagnotte;
                suspended[j+1] = tempSuspended;
                isAdmin[j+1] = tempIsAdmin;
            }
        }
    }
    return 0;
}

/**
 * @brief Tri des articles par ordre croissant en fonction de leur référence.
 * @param reference: tableau des références des articles
 * @param weight: tableau des poids des articles
 * @param volume: tableau des volumes des articles
 * @param unitPrice: tableau des prix à l'unité des articles
 * @param tlog: taille logique des tableaux des articles
*/
int sortArticle(int reference[], float weight[], float volume[], float unitPrice[], int tlog)
{
    int i, j, tempReference;
    float tempWeight, tempVolume, tempUnitPrice;
    for (i=0; i<tlog; i++)
    {
        for (j=0; j<tlog; j++)
        {
            if (reference[j] > reference[j+1])
            {
                tempReference = reference[j];
                tempWeight = weight[j];
                tempVolume = volume[j];
                tempUnitPrice = unitPrice[j];

                reference[j] = reference[j+1];
                weight[j] = weight[j+1];
                volume[j] = volume[j+1];
                unitPrice[j] = unitPrice[j+1];

                reference[j+1] = tempReference;
                weight[j+1] = tempWeight;
                volume[j+1] = tempVolume;
                unitPrice[j+1] = tempUnitPrice;
            }
        }
    }
    return 0;
}
