/**
 * @file main.h
 * @brief Header possédant les inclusions de module, déclarations de constante & prototypes de fonction.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h> // Fonction sleep

// Constantes
#define tmaxArticles 500
#define tmaxClients 750

//! GESTION DES ERREURS & CODES
void errorHandling(int error);

//! DEBUG
void debugMenu(int tabReference[], float tabWeight[], float tabVolume[], float unitPrice[], int clientID[], float cagnotte[], int suspended[], int isAdmin[], int tlogArticle, int tlogClient);

//! TRAITEMENT DES FICHIERS
void chargeDonnees(int *tLogArticle, int *tLogClient,int reference[], float weight[], float volume[], float unitPrice[], int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[]);
int chargeArticles(int reference[], float weight[], float volume[], float unitPrice[]);
int chargeClients(int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[]);

void sauvegardeDonnees(int tLogArticle, int tLogClient, int reference[], float weight[], float volume[], float unitPrice[], int clientID[], int clientPassword[], float cagnotte[], int isAdmin[], int suspended[]);
void sauvegardeArticles(int reference[], float weight[], float volume[], float unitPrice[], int tLogArticle);
void sauvegardeClients(int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[], int tLogClient);

//! DECALAGES TABLEAUX
void decalageADroiteInt(int tab[], int index, int tlog);
void decalageAGaucheInt(int tab[], int index, int tlog);
void decalageADroiteFloat(float tab[], int index, int tlog);
void decalageAGaucheFloat(float tab[], int index, int tlog);

//! GESTION CLIENTS
void inputClient(int id, int passwd, int index, int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[], int *tlog);
void modifyClient(int clientID[], float cagnotte[], int suspended[], int isAdmin[], int tlog);
void deleteClient(int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[], int *tlog);

//! GESTION STOCKS
void inputItem(int tabReference[], float tabWeight[], float tabVolume[], float unitPrice[], int *tlog);
void deleteItem(int tabReference[], float tabWeight[], float tabVolume[], float unitPrice[], int *tlog);

//! AFFICHAGE DES DONNEES
void displayItem(int tabReference[], float weight[], float volume[], float unitPrice[], int tlog);
void displayItemList(int reference[], float weight[], float volume[], float unitPrice[], int log);
void displayClient(int clientID[], float cagnotte[], int suspended[], int isAdmin[], int tlog);
void displayClientList(int clientID[], float cagnotte[], int suspended[], int isAdmin[], int tlog);

//! RECHERCHE
int searchTab(int targetTab[], int valSearched, int tLog, int *found);

//! PANIER
void clientConstraint(float *weight, float *volume, float *price);
int constraintExceeded(float weightConstraint, float volumeConstraint, float *priceConstraint, float *cagnotte, float tabWeight[], float tabVolume[], float tabPrice[], int tabItemRef[], int tabBasketRef[], int tabBasketQuantity[], int tlogItem, int tlogBasket);
void basket_add(int tab_reference[], float unitPrice[], float *cagnotte, int  basket_tab_ref[], int basket_tab_qte[], int tlogItem, int *tlog_basket);
void display_basket(int basket_tab_ref[], int tabItemRef[], int basket_qte[], float tabWeight[], float tabVolume[], float cagnotte, float tabUnitPrice[], int tlogItem, int tlog_basket);
void reinit_basket(int *tlog_basket);
void basket_del_article(int basket_tab_ref[], int basket_tab_qte[], int *tlog_basket);

//! LOGIN
int login(int clientID[], int clientPassword[], int isAdmin[], int suspended[], int tlog, int *index);
int signup(int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[], int *tlog);

//! MENUS
void adminMenu(int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[], int tabReference[], float tabWeight[], float tabVolume[], float unitPrice[], int *tlogItem, int *tlogClient);
void clientMenu(float *weight, float *volume, float *price, int tabItemRef[], float unitPrice[], float *cagnotte, int basket_tab_ref[], int basket_tab_qte[], float tabWeight[], float tabVolume[], int tlogItem, int *tlogBasket);
void opposition(int clientID[], int clientPassword[], int suspended[], int tlogClient);