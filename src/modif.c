/**
 * @file modif.c
 * @brief Fonctions de modification des données (ajout, suppression, modification)
*/

#include "main.h"

/**
 * @brief Ajout d'un client dans les tableaux
 * @param id: Tableau des identifiants des clients
 * @param passwd: Tableau des mots de passe des clients
 * @param index: Index du client à ajouter
 * @param clientID: Tableau des identifiants des clients
 * @param clientPassword: Tableau des mots de passe des clients
 * @param cagnotte: Tableau des cagnottes des clients
 * @param suspended: Tableau de l'état du compte des clients
 * @param isAdmin: Tableau de l'état d'administrateur des clients
 * @param tlog: Taille logique des tableaux
 * @return 0 si tout s'est bien passé, -2 si la taille physique du tableau est dépassée
*/
void inputClient(int id, int passwd, int index, int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[], int *tlog)
{
    // Vérification du dépassement de la taille physique du tableau
    if (*tlog == tmaxClients)
    {
        errorHandling(-2);
        return;
    }
    
    // Décalage
    decalageADroiteInt(clientID, index, *tlog);
    decalageADroiteFloat(cagnotte, index, *tlog);
    decalageADroiteInt(suspended, index, *tlog);
    decalageADroiteInt(isAdmin, index, *tlog);

    clientID[index] = id;
    clientPassword[index] = passwd;
    cagnotte[index] = 0;
    suspended[index] = 0;
    isAdmin[index] = 0;

    *tlog++;
}

/**
 * @brief Modification des données d'un client
 * @param clientID: Tableau des identifiants des clients
 * @param cagnotte: Tableau des cagnottes des clients
 * @param suspended: Tableau de l'état du compte des clients
 * @param isAdmin: Tableau de l'état d'administrateur des clients
 * @param tlog: Taille logique des tableaux
 * @return void
*/
void modifyClient(int clientID[], float cagnotte[], int suspended[], int isAdmin[], int tlog)
{
    int index, numClient, found;
    float montant;
    char modif;
    printf("Entrez le numéro du client dont vous voulez modifier les données: ");
    scanf("%d%*c", &numClient);
    while (numClient < 0)
    {
        printf("Erreur, le numéro du client doit être positif, entrez le numéro du client ou 0 si vous souhaitez arrêter la saisie: ");
        scanf("%d%*c", &numClient);
    }
    index = searchTab(clientID, numClient, tlog, &found);
    while(found == 0)
    {
        printf("Cet identifiant client n'existe pas, réessayer ou tapez 0 si vous souhaitez arrêter la saisie: ");
        scanf("%d%*c", &numClient);
        index = searchTab(clientID, numClient, tlog, &found);
    }
    printf("Entrez la modification voulue (s pour suspendre ou réactiver, c pour ajouter à la cagnotte, a pour activer ou désactiver les droits administrateurs): ");
    scanf("%c%*c", &modif);
    while (modif != 's' && modif != 'c' && modif != 'a')
    {
        printf("Erreur, le choix doit être 'c' ou 's', entrez la modification voulue (s pour suspendre ou réactiver, c pour ajouter à la cagnotte, a pour activer ou désactiver les droits administrateurs): ");
        scanf("%c%*c", &modif);
    }
    if (modif == 's')
        if (suspended[index] == 0)
            suspended[index] = 1;
        else if (suspended[index] == 1)
            suspended[index] = 0;
    else if (modif == 'c')
    {
        printf("Entrez le montant à ajouter à la cagnotte: ");
        scanf("%f%*c", &montant);
        cagnotte[index] += montant;
    }
    else if (modif == 'a')
        if (isAdmin[index] == 0)
            isAdmin[index] = 1;
        else if (isAdmin[index] == 1)
            isAdmin[index] = 0;
}

/**
 * @brief Ajout d'un ou plusieurs articles dans les tableaux d'articles (reference, poids, volume et prix)
 * @param tabReference: tableau des références des articles
 * @param weight: tableau des poids des articles
 * @param volume: tableau des volumes des articles
 * @param unitPrice: tableau des prix à l'unité des articles
 * @param tlog: taille logique des tableaux des articles
 * @return void
 * @warning La saisie s'arrête lorsque l'utilisateur entre 0
*/
void inputItem(int tabReference[], float tabWeight[], float tabVolume[], float unitPrice[], int *tlog)
{
    int reference = -1, index, found;
    float weight, volume, price;
    while(reference != 0)
    {
        printf("Entrez la référence du produit ou 0 si vous souhaitez arrêter la saisie: ");
        scanf("%d%*c", &reference);
        while (reference < 0)
        {
            printf("Erreur: la référence doit être positive, entrez la référence du produit ou 0 si vous souhaitez arrêter la saisie: ");
            scanf("%d%*c", &reference);
        }

        index = searchTab(tabReference, reference, *tlog, &found);

        while(found == 1 && reference != 0)
        {
            printf("Cette référence existe déjà, réessayer ou tapez 0 si vous souhaitez arrêter la saisie: ");
            scanf("%d%*c", &reference);
            index = searchTab(tabReference, reference, *tlog, &found);
        }

        if (reference != 0)
        {
            printf("Entrez le poids du produit: ");
            scanf("%f%*c", &weight);
            while (weight < 0)
            {
                printf("Erreur: le poids doit être positif, entrez le poids du produit: ");
                scanf("%f%*c", &weight);
            }
            printf("Entrez le volume du produit: ");
            scanf("%f%*c", &volume);
            while (volume < 0)
            {
                printf("Erreur: le volume doit être positif, entrez le volume du produit: ");
                scanf("%f%*c", &volume);
            }
            printf("Entrez le prix du produit: ");
            scanf("%f%*c", &price);
            while (price < 0)
            {
                printf("Erreur: le prix doit être positif, entrez le prix du produit: ");
                scanf("%f%*c", &price);
            }
            
            //Décalage
            decalageADroiteInt(tabReference, index, *tlog);
            decalageADroiteFloat(tabWeight, index, *tlog);
            decalageADroiteFloat(tabVolume, index, *tlog);
            decalageADroiteFloat(unitPrice, index, *tlog);

            tabReference[index] = reference;
            tabWeight[index] = weight;
            tabVolume[index] = volume;
            unitPrice[index] = price;

            *tlog++;
        }
    }
}

/**
 * @brief Suppression d'un ou plusieurs articles dans les tableaux d'articles (reference, poids, volume et prix)
 * @param tabReference: tableau des références des articles
 * @param weight: tableau des poids des articles
 * @param volume: tableau des volumes des articles
 * @param unitPrice: tableau des prix à l'unité des articles
 * @param tlog: taille logique des tableaux des articles
 * @return void
 * @warning La saisie s'arrête lorsque l'utilisateur entre 0
*/
void deleteItem(int tabReference[], float tabWeight[], float tabVolume[], float unitPrice[], int *tlog)
{
    int reference = -1, index, i, found;
    while (reference != 0)
    {
        printf("Entrez la référence du produit à supprimer ou 0 si vous souhaitez arrêter la saisie: ");
        scanf("%d%*c", &reference);
        while (reference < 0)
        {
            printf("Erreur: la référence doit être positive, entrez la référence du produit ou 0 si vous souhaitez arrêter la saisie: ");
            scanf("%d%*c", &reference);
        }
        index = searchTab(tabReference, reference, *tlog, &found);
        while(found == 0)
        {
            printf("Cette référence n'existe pas, réessayer ou tapez 0 si vous souhaitez arrêter la saisie: ");
            scanf("%d%*c", &reference);
            index = searchTab(tabReference, reference, *tlog, &found);
        }
        
        decalageAGaucheInt(tabReference, index, *tlog);
        decalageAGaucheFloat(tabWeight, index, *tlog);
        decalageAGaucheFloat(tabVolume, index, *tlog);
        decalageAGaucheFloat(unitPrice, index, *tlog);
        
        *tlog--;
    }
}

/**
 * @brief Suppression d'un ou plusieurs clients dans les tableaux des clients (clientID, clientPassword, cagnotte, suspended et isAdmin)
 * @param clientID: tableau des identifiants des clients
 * @param cagnotte: tableau des cagnottes des clients
 * @param suspended: tableau de l'état du compte des clients
 * @param isAdmin: tableau de l'état d'administrateur des clients
 * @param tlog: taille logique des tableaux des clients
 * @return void
 * @warning La saisie s'arrête lorsque l'utilisateur entre 0
*/
void deleteClient(int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[], int *tlog)
{
    int numClient = -1, index, i, found;
    while (numClient != 0)
    {
        printf("Entrez l'identifiant du client à supprimer ou 0 si vous souhaitez arrêter la saisie: ");
        scanf("%d%*c", &numClient);
        while (numClient < 0)
        {
            printf("Erreur, le numéro du client doit être positif, entrez le numéro du client ou 0 si vous souhaitez arrêter la saisie: ");
            scanf("%d%*c", &numClient);
        }
        index = searchTab(clientID, numClient, *tlog, &found);
        while(found == 0)
        {
            printf("Cet identifiant client n'existe pas, réessayer ou tapez 0 si vous souhaitez arrêter la saisie: ");
            scanf("%d%*c", &numClient);
            index = searchTab(clientID, numClient, *tlog, &found);
        }
        
        decalageAGaucheInt(clientID, index, *tlog);
        decalageAGaucheInt(clientPassword, index, *tlog);
        decalageAGaucheFloat(cagnotte, index, *tlog);
        decalageAGaucheInt(suspended, index, *tlog);
        decalageAGaucheInt(isAdmin, index, *tlog);

        *tlog--;
    }
}