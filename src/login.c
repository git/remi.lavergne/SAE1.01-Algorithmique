/**
 * @file login.c
 * @brief Fonctions de connexion et d'inscription
*/

#include "main.h"

/**
 * @brief Fonction de connexion
 * @param clientID: Tableau des identifiants client
 * @param isAdmin: Tableau des statuts Administrateur des clients
 * @param tlog: Taille logique des Tableaux
 * @param suspended: Tableau des états de compte client
 * @return Est Admin ? (1 > Oui, 0 > Non, -1 > Quitter)
*/
int login(int clientID[], int clientPassword[], int isAdmin[], int suspended[], int tlog, int *index)
{
    int id, password, found, tentative=3;
    printf("Entrez votre identifiant: ");
    scanf("%d%*c", &id);
    while (id < 0)
    {
        printf("Erreur, l'identifiant doit être positif, entrez votre identifiant: ");
        scanf("%d%*c", &id);
    }
    *index = searchTab(clientID, id, tlog, &found);
    while(found == 0)
    {
        printf("Erreur, l'identifiant n'existe pas, entrez votre identifiant: ");
        scanf("%d%*c", &id);
        if (id == 0)
            return -1;

        // Nombre de tentatives restantes
        tentative--;
        if (tentative == 0)
        {
            printf("Nombre de tentatives dépassé, retour au menu principal.\n");
            return -1;
        }
        printf("Il vous reste %d tentatives.\n", tentative);

        *index = searchTab(clientID, id, tlog, &found);
    }

    if(suspended[*index] == 1)
    {
        errorHandling(-10);
        return -10;
    }

    printf("Entrez votre mot de passe: ");
    scanf("%d%*c", &password);
    while (password < 0)
    {
        printf("Erreur, le mot de passe doit être positif, entrez votre mot de passe: ");
        scanf("%d%*c", &password);
    }

    if (password == clientPassword[*index])
    {
        if (isAdmin[*index] == 1)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    else
    {
        errorHandling(-9);
        return -9;
    }
}

/**
 * @brief Fonction d'inscription
 * @param clientID: Tableau des identifiants client
 * @param clientPassword: Tableau des mots de passe client
 * @param cagnotte: Tableau des cagnottes client
 * @param suspended: Tableau des états de compte client
 * @param isAdmin: Tableau des statuts Administrateur des clients
 * @param tlog: Taille logique des Tableaux
 * @return 0 > Tout s'est bien passé, -1 > L'utilisateur a quitté, -2 > La taille physique du tableau est dépassée
*/
int signup(int clientID[], int clientPassword[], float cagnotte[], int suspended[], int isAdmin[], int *tlog)
{
    int id, password, index, found;
    printf("Entrez votre identifiant ou 0 si vous souhaitez quitter: ");
    scanf("%d%*c", &id);
    while (id < 0)
    {
        printf("Erreur, l'identifiant doit être positif, entrez votre identifiant ou 0 si vous souhaitez quitter: ");
        scanf("%d%*c", &id);
    }
    if (id == 0)
    {
        errorHandling(-1);
        return -1;
    }

    index = searchTab(clientID, id, *tlog, &found);
    while(found == 1)
    {
        printf("Erreur, l'identifiant existe déjà, entrez votre identifiant ou 0 si vous souhaitez quitter: ");
        scanf("%d%*c", &id);
        if (id == 0)
        {
            errorHandling(-1);
            return -1;
        }
        index = searchTab(clientID, id, *tlog, &found);
    }

    printf("Entrez votre mot de passe: ");
    scanf("%d%*c", &password);
    while (password < 0)
    {
        errorHandling(-6);
        printf("Erreur, le mot de passe doit être positif, entrez votre mot de passe: ");
        scanf("%d%*c", &password);
    }

    inputClient(id, password, index, clientID, clientPassword, cagnotte, suspended, isAdmin, tlog);
    return 0;
}