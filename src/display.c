/**
 * @file display.c
 * @brief Fonctions d'affichage des articles et des clients
*/

#include "main.h"

/**
 * @brief Affichage d'un seul article
 * @param tabReference: tableau des références des articles
 * @param weight: tableau des poids des articles
 * @param volume: tableau des volumes des articles
 * @param unitPrice: tableau des prix à l'unité des articles
 * @param tlog: taille logique des tableaux des articles
 * @return void
*/
void displayItem(int tabReference[], float weight[], float volume[], float unitPrice[], int tlog)
{
	int reference, index, found;
	printf("Entrez la référence de l'article cherché: ");
	scanf("%d", &reference);
	while (reference < 0)
	{
		printf("Erreur, la référence doit être positive, entrez la référence de l'article cherché: ");
		scanf("%d", &reference);
	}
	index = searchTab(tabReference, reference, tlog, &found);
	if (index == -1)
	{
		printf("La référence cherchée n'existe pas");
		exit(1);
	}
	printf("reference\tpoids\tvolume\tprix à l'unité");
	printf("%d\t%f\t%f\t%f", tabReference[index], weight[index], volume[index], unitPrice[index]);
}

/**
 * @brief Affichage de la liste des articles
 * @param tabReference: tableau des références des articles
 * @param weight: tableau des poids des articles
 * @param volume: tableau des volumes des articles
 * @param unitPrice: tableau des prix à l'unité des articles
 * @param tlog: taille logique des tableaux des articles
 * @return void
*/
void displayItemList(int reference[], float weight[], float volume[], float unitPrice[], int tlog)
{
	int i;
	printf("reference\tpoids\tvolume\tprix à l'unité");
	for (i=0; i<tlog; i++)
	{
		printf("%d\t%f\t%f\t%f", reference[i], weight[i], volume[i], unitPrice[i]);
	}
}

/**
 * @brief Affichage d'un seul client
 * @param clientID: tableau des identifiants des clients
 * @param cagnotte: tableau des cagnottes des clients
 * @param suspended: tableau de l'état du compte des clients
 * @param isAdmin: tableau de l'état d'administrateur des clients
 * @param tlog: taille logique des tableaux des clients
 * @return void
*/
void displayClient(int clientID[], float cagnotte[], int suspended[], int isAdmin[], int tlog)
{
	int id, index, found;
	printf("Entrez le numéro du client cherché: ");
	scanf("%d", &id);
	while (id < 0)
	{
		printf("Erreur, le numéro du client doit être positif, entrez le numéro du client cherché: ");
		scanf("%d", &id);
	}
	index = searchTab(clientID, id, tlog, &found);
	if (index == -1)
	{
		printf("Le client cherché n'existe pas");
		exit(1);
	}
	printf("num Client\tcagnotte\tsuspended\tisAdmin");
	printf("%d\t%f\t%d\t%d", clientID[index], cagnotte[index], suspended[index], isAdmin[index]);
}

/**
 * @brief Affichage de la liste des clients
 * @param clientID: tableau des identifiants des clients
 * @param cagnotte: tableau des cagnottes des clients
 * @param suspended: tableau de l'état du compte des clients
 * @param isAdmin: tableau de l'état d'administrateur des clients
 * @param tlog: taille logique des tableaux des clients
 * @return void
*/
void displayClientList(int clientID[], float cagnotte[], int suspended[], int isAdmin[], int tlog)
{
	int i;
	printf("num Client\tcagnotte\tsuspended\tisAdmin");
	for (i=0; i<tlog; i++)
	{
		printf("%d\t%f\t%d\t%d", clientID[i], cagnotte[i], suspended[i], isAdmin[i]);
	}
}

/**
 * @brief Ajoute un article au panier et calcule les différents attributs pour la fonction `display_basket`
 * @param basket_tab_ref: tableau des références du panier
 * @param basket_tab_qte: tableau de la quantité de l'article du panier
 * @param weight: tableau des poids des articles
 * @param volume: tableau du volume des articles
 * @param unitPrice: tableau du prix unitaire des articles
 * @param cagnotte: valeur de la cagnotte
 * @param tlog: taille logique du tableau `tab_reference`
 * @param tlog_basket: taille logique du panier
 * @return void
*/
void display_basket(int basket_tab_ref[], int tabItemRef[], int basket_qte[], float tabWeight[], float tabVolume[], float cagnotte, float tabUnitPrice[], int tlogItem, int tlog_basket)
{
	float total_weight_line[tlog_basket], total_volume_line[tlog_basket], total_price_line[tlog_basket];
	float totalWeight = 0, totalVol = 0, total_price = 0, weight[tmaxArticles], volume[tmaxArticles], unitPrice[tmaxArticles];
	int i, found, index;

		
	printf("Récapitulatif de votre panier:\n");
	printf("Référence\tQuantité\tPoids\tVolume\tPrix unitaire\tPoids Total\tVol Total\tPrix Total\n");

	for (i=0; i<tlog_basket; i++)
	{
		index = searchTab(tabItemRef, basket_tab_ref[i], tlogItem, &found);
		if (found == 1)
		{
			weight[i] = tabWeight[index];
			volume[i] = tabVolume[index];
			unitPrice[i] = tabUnitPrice[index];
		}
		total_weight_line[i] = basket_qte[i]*weight[i];
		total_volume_line[i] = basket_qte[i]*volume[i];
		total_price_line[i] = basket_qte[i]*unitPrice[i];
		totalWeight += total_weight_line[i];
		totalVol += total_volume_line [i];
		total_price += total_price_line[i];
		printf("%d\t%d\t%f\t%f\t%f\t%f\t%f\t%f\n", basket_tab_ref[i], basket_qte[i], weight[i], volume[i], unitPrice[i], total_weight_line[i], total_volume_line[i], total_price_line[i]);
	}
	
	printf("Prix total: %f€\n", total_price);
	printf("Poids total: %fkg\n", totalWeight);
	printf("Volume total: %fm³\n", totalVol);
	printf("Cagnotte:%.2f€", cagnotte);
}
