/**
 * @file main.c
 * @brief Fonctions principales du programme
 * 
 * Contient les fonctions principales du programme, notamment le menu principal.
 * La fonction main() se trouve dans ce fichier et est executée au lancement.
 * La fonction global() est la fonction principale du programme.
*/
#include "main.h"

/**
 * @brief Affichage du menu principal
 * @return Choix de l'utilisateur
*/
int displayMenu(void)
{
    int choice;
    
    printf("\nTapez sur Entrée pour continuer...");
    if (getchar() == '\n') // getchar() pour récupérer le caractère entré par l'utilisateur
    {
        system("clear"); // Clear le terminal
    }

    printf("\n=================================\n");
    printf("          Menu Principal\n");
    printf("=================================\n");
    printf("1. \u25B6 Connexion\n");
    printf("2. \u25B6 Inscription\n");
    printf("3. \u25B6 Faire opposition\n");
    printf("4. \u274C Quitter\n");
    printf("9. \u26A0 Débug\n");
    printf("=================================\n");
    printf("\u00A9 2023 - Pascal Inc.\n\n");
    printf("Votre choix : ");
    scanf("%d%*c", &choice);

    system("clear");

    return choice;
}

/**
 * @brief Fonction globale du programme
 * Crée les variables et les tableaux nécessaires au programme,
 * charge les données depuis les fichiers, affiche le menu principal
 * et appelle les fonctions nécessaires en fonction du choix de l'utilisateur.
*/
void global(void)
{
    // Déclaration des variables
    int tLogItem, tLogClient, tlogBasket, choice, status, currentUser, index;
    int reference[tmaxArticles], clientID[tmaxClients], clientPassword[tmaxClients], suspended[tmaxClients], isAdmin[tmaxClients], basket_tab_ref[tmaxArticles], basket_tab_qte[tmaxArticles];
    float weight[tmaxArticles], volume[tmaxArticles], unitPrice[tmaxArticles], cagnotte[tmaxClients];
    // Chargement de toute les données
    chargeDonnees(&tLogItem, &tLogClient, reference, weight, volume, unitPrice, clientID, clientPassword, cagnotte, suspended, isAdmin);
    // Affichage du menu
    choice = displayMenu();

    while(choice != 4)
    {
        switch(choice)
        {
            case 1:
                status = login(clientID, clientPassword, isAdmin, suspended, tLogClient, &index);
                if(status == 1)
                {
                    adminMenu(clientID, clientPassword, cagnotte, suspended, isAdmin, reference, weight, volume, unitPrice, &tLogItem, &tLogClient);
                }
                else if(status == 0)
                {
                    clientMenu(&weight[index], &volume[index], &unitPrice[index], reference, unitPrice, &cagnotte[index], basket_tab_ref, basket_tab_qte, weight, volume, tLogItem, &tlogBasket);
                }
                else
                {
                    printf("Connexion échouée.\nRetour au menu principal...\n");
                    sleep(2);
                }
                break;
            case 2:
                status = signup(clientID, clientPassword, cagnotte, suspended, isAdmin, &tLogClient);
                if(status == 0)
                {
                    printf("Inscription réussie, vous pouvez vous connecter.\nRetour au menu principal...\n");
                    sleep(2);
                }
                else
                {
                    printf("Inscription échouée.\nRetour au menu principal...\n");
                    sleep(2);
                }
                break;
            case 3:
                opposition(clientID, clientPassword, suspended, tLogClient);
                break;
            case 4:
                sauvegardeDonnees(tLogItem, tLogClient, reference, weight, volume, unitPrice, clientID, clientPassword, cagnotte, isAdmin, suspended);
                break;
            case 9:
                debugMenu(reference, weight, volume, unitPrice, clientID, cagnotte, suspended, isAdmin, tLogItem, tLogClient);
                break;
            default:
                printf("Erreur, veuillez entrer un choix valide.\n");
                choice = displayMenu();
                break;
        }
        choice = displayMenu();
    }
    sauvegardeDonnees(tLogItem, tLogClient, reference, weight, volume, unitPrice, clientID, clientPassword, cagnotte, isAdmin, suspended);
}

/**
 * @brief Fonction principale du programme
 * @return 0 si tout s'est bien passé et que le programme s'est terminé correctement
*/
int main(void)
{
    global();
    return 0;
}