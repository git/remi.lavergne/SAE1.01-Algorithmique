
SAÉ 1.01 - Implémentation d'un besoin client
=================================

Par:
Rémi LAVERGNE - Julien ABADIE - Louis GERMAIN
Groupe 7/8 - 1A 2023-2024
<br>

**Répartition du Travail**
-----------------------

**Rémi LAVERGNE :** Fonctions de traitement des fichiers (tris, chargement, sauvegarde), traitement des erreurs, connexion/inscription & fonction globale.<br>
**Louis GERMAIN :** Fonctions de vérification des contraintes, ajout/modification/suppression des clients/articles, menus annexes (client,admin), fonction globale.<br>
**Julien ABADIE :** Fonctions d'affichage (clients,articles,panier), gestion du panier et définition des contraintes, fonction globale.
<br><br>
Documentation réalisée par chacun.
<br>

**Documentation (Doxygen)**
-------------------------

📚 La documentation est générée et hebergée sur [**> CodeDoc <**](https://codefirst.iut.uca.fr/documentation/remi.lavergne/SAE1.01-Algorithmique/doxygen/) .
<br>

**Composition des fichiers sources**
--------------------------------

Pour les autres fichiers, voir la Documentation.<br>

- *main.c* >> Juste la fonction main, se lançant lors de l'exécution du programme. Avec elle, le menu principal et la fonction globale, coeur du programme.
- *main.h* >> Tous les prototypes des fonctions réalisées, ainsi que les inclusions de module et déclaration de constantes.

<br>

- *articles.txt* >> Fichier texte composé de 4 colonnes : référence | poids | volume | prix unitaire
- *clients.txt* >> Fichier texte composé de 5 colonnes dans l'ordre qui suit :
  identifiant | mot de passe | cagnotte | suspendu (0=actif ; 1=suspendu) | rôle (0=client ; 1=administrateur)

<br>

**Fonctionnement**
------------

Lors de l'exécution du programme, le menu principal s'affiche. Il est composé de 4 choix : connexion, inscription, opposition et quitter. 
Le choix 1 permet de se connecter, le choix 2 de s'inscrire, le choix 3 de récupérer l'accès à sa carte suspendue et le choix 4 de quitter le programme. 
- Si l'utilisateur se connecte, il est redirigé vers le menu client ou administrateur selon son rôle. 
- Si l'utilisateur s'inscrit, il est redirigé vers le menu principal.
- Si l'utilisateur récupère l'accès à sa carte suspendue, il est redirigé vers le menu principal. 
- Si l'utilisateur quitte le programme, il est déconnecté et le programme se ferme après la sauvegarde des données.

<br>

Traces d'Exécution
===============

Trace #1 : https://youtu.be/MiiCGtzQzEM

<br>

Jeux de données
==============

articles.txt
```
1 33.80 1.62 64.50
2 45.97 1.21 193.57
3 91.03 0.57 79.36
4 59.32 0.71 100.99
5 34.18 1.43 42.44
6 75.43 2.43 57.86
7 26.11 2.32 155.97
8 31.89 0.92 190.94
9 90.74 0.59 61.87
10 15.63 1.81 148.95
11 62.08 1.47 185.78
12 84.46 2.00 49.14
13 71.74 0.46 177.15
14 81.59 1.08 69.27
15 76.09 1.28 92.17
16 50.92 2.37 28.14
17 73.27 1.07 142.01
18 99.08 2.16 5.51
19 76.33 1.28 66.87
20 47.91 1.28 14.79
21 49.92 0.92 86.80
22 88.98 1.18 127.61
23 11.72 1.97 189.64
24 22.72 0.56 106.91
25 99.43 1.61 52.19
26 73.17 1.78 112.36
27 17.10 2.26 72.91
28 96.59 2.43 43.50
29 16.39 1.36 77.74
30 47.78 1.85 36.03
31 12.49 0.96 170.42
32 53.07 0.32 83.83
33 42.26 0.72 198.57
34 86.22 1.03 195.66
35 46.83 0.37 23.29
36 95.86 1.14 163.49
37 21.67 1.08 188.90
38 16.15 0.32 169.18
39 39.76 2.45 111.22
40 16.23 2.15 142.99
41 13.15 1.05 58.28
42 46.66 0.30 45.97
43 67.64 2.28 23.28
44 98.52 1.71 10.28
45 96.39 1.11 59.34
46 96.30 0.67 159.19
47 45.94 0.62 169.54
48 20.28 1.78 145.46
49 44.08 1.57 169.84
50 80.91 2.13 67.70

```

clients.txt
```
1 7062 115.95 0 1
2 9392 446.75 0 0
3 7920 418.33 1 0
4 371 288.01 0 0
5 3762 175.85 0 0
6 3427 404.05 0 0
7 6861 148.23 0 0
8 57 201.61 0 0
9 6877 348.38 1 0
10 2497 66.83 0 0
11 5511 34.30 0 0
12 3377 236.79 0 0
13 8021 209.12 0 0
14 7696 283.08 0 0
15 4792 201.22 1 1
16 1423 334.40 0 0
17 919 360.44 0 0
18 3297 105.79 0 0
19 3569 410.50 0 0
20 457 69.42 0 0
21 8400 146.92 0 0
22 6401 205.69 0 0
23 4585 310.51 0 0
24 6228 406.27 0 0
25 635 12.86 0 0
26 9770 486.48 0 0
27 6919 67.96 0 0
28 6703 252.47 0 0
29 708 52.76 0 0
30 4440 425.78 0 0
31 5768 433.09 0 0
32 9810 442.02 0 0
33 6220 256.28 0 0
34 5317 473.44 0 0
35 3284 370.82 0 0
36 2998 426.58 0 0
37 8975 425.60 0 0
38 5877 106.48 0 0
39 1957 343.13 0 0
40 9020 378.99 0 0
41 9675 163.61 0 0
42 8637 288.33 0 0
43 5125 350.72 0 0
44 1318 79.63 0 0
45 2598 251.03 0 0
46 8815 302.61 0 0
47 8916 143.71 0 0
48 1503 452.92 0 0
49 7690 435.32 0 0
50 2999 57.94 0 0
```